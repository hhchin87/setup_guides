# Guide to installing the latest Nvidia Drivers + CUDA  + cudnn with  on Ubuntu 16.04  LTS
This is guide to instal the latest CUDA toolkit. If you do not need the latest versions, you can check out the other guide on using the ubuntu repositories to install cuda

**_Alert!_** 
* This guide is not for Nvidia Optimus enabled cards. Those cards are notoriously to install.  

### References
* https://github.com/zcn17/install-guides/blob/master/caffe_setup.md
* http://askubuntu.com/questions/762254/why-do-i-get-required-key-not-available-when-install-3rd-party-kernel-modules

### Generic Setup Steps
1. install nvidia drivers
2. install cuda toolkit
3. install cudnn

_Note_
for this guide we would be install
1. Nvidia driver: NVIDIA-Linux-x86_64-367.35
2. Cuda toolkit: cuda_8.0.27
3. CuDNN: 5.0

## Preparation Work
0. Download the latest nvidia drivers from the official [Nvidia page](http://www.nvidia.com/Download/index.aspx?lang=en-us). <br/> *This step is crucial!* You might not be able to login back into the desktop if a kernel update breaks your driver!
1. ensure that your ubuntu distribution is up to date 
    ```
    sudo apt-get update
    sudo apt-get dist-upgrade
    ```
        
2. As of Ubuntu 16.04, kernel drivers are needed to be signed due to secure boot. Though disabling secure boot is a viable option, it is not recommened. DKMS and mokutil are needed for signing the drivers
    ```
    sudo apt-get install dkms mokutil
    ```
    
3. Prepare the private and public keys required for signing. Store it at your home directory for ease during installation but you should stash it somewhere once it is done.
    ```
    openssl req -new -x509 -newkey rsa:2048 -keyout MOK.priv -outform DER -out MOK.der -nodes -days 36500 -subj "/CN=Keys for drivers/"
    ```

## Install Nvidia Drivers
1. The nvidia drivers should been downloaded.
2. Press __ctrl+atrl+f4__ (or any of the f1-f6, f7 is the one current view) to go into a terminal
3. Stop the display manager that's running. By default, ubuntu uses lightdm
    ```
    sudo service lightdm stop
    ```
    
    i. **_Potential Issue: There are previous versions of the nvidia drivers messing up the installation_** <br/>
    remove all traces of nvidia related stuff.
    ```
    sudo apt-get remove nvidia* && sudo apt-get autoremove
    ```
    
4. Install the nvidia drivers. Goto the download folder
    ```
    sudo ./NVIDIA-Linux-x86_64-367.35.run -s --module-signing-secret-key=/home/user/MOK.priv --module-signing-public-key=/home/user/MOK.der
    ```
    
    ii. **_Potential Issue: Default Drivers Nouveau does not unload_** <br/>
    This is a tricky issues that arise when there are other drivers such as the onboard integrated graphics card which prevents the installed driver from unloading when `lightdm stop` is called. You have to manually blacklist the Nouveau driver and reboot to prevent it from being loaded. The latest Nvidia drivers installers will do this step if it is installed successfully. <br/>
    Create a blacklist file so that future modifications to the kernel will not cause nouveau drivers again. 
    ```
    sudo nano /etc/modprobe.d/blacklist-nouveau.conf
    ```
    
    and add these lines at the end before saving:
    ```
    blacklist nouveau
    blacklist lbm-nouveau
    options nouveau modeset=0
    alias nouveau off
    alias lbm-nouveau off
    ```
    
    Exclude the Nouveau drivers and reboot. This might take a while.
    ```
    echo options nouveau modeset=0 | sudo tee -a /etc/modprobe.d/nouveau-kms.conf
    update-initramfs -u
    sudo reboot
    ```
    
    You should reboot into a low resolution version of ubuntu. Dont panic. Hit __ctrl+alt+f4__ and return to step 4.
    
5. The following will add public key into the boot process. You would have a create a onetime password to be used upon reboot. <br/>
A graphical guide of the reboot process can be seen [here](https://sourceware.org/systemtap/wiki/SecureBoot)

    ```
    sudo mokutil --import MOK.der
    sudo reboot
    ```
6. Upon successful rebooting and loading of the Nvidia drivers, you will be able to login into your desktop.  

    iii. **_Potential Issue: Unable to log in or login cycle_** <br/>
    This is due the driver not being able to load into the kernel or the user profile files such as .Xauthority having wrong permissions.
    
    iv. **_Potential Issue: Blackscreen, no ubuntu login screen shown_** <br/>
    The driver have issue with the graphics card. 
    
7. run the nvidia driver setting in a normal terminal on the desktop (shortcut *ctrl+atl+t*)
    ```
    sudo nvidia-xconfig
    ```
    
## Install CUDA toolkit
1. Download the cuda toolkit at [Nvidia page](https://developer.nvidia.com/cuda-toolkit). <br/>
direct link to [CUDA 8.0 rc](https://developer.nvidia.com/compute/cuda/8.0/rc/local_installers/cuda_8.0.27_linux-run). Nvidia signup required<br/>
**DO NOT install the Nvidia Drivers bundled with the toolkit!**

2. Run the installer
    ```
    chmod +x cuda_8.0.27_linux.run
    sudo ./cuda_8.0.27_linux.run
    ```
    
    i. **_Potential Issue: Installer complains about GCC_** <br/>
    GCC>=5.0 is not officially supported as of CUDA 8.0 release candidate (rc)
    ```
    sudo ./cuda_8.0.27_linux.run --override
    ```
    
    Add the following line to your shell configuration file (ubuntu's default is ~/.bashrc) so that other programs can find the cuda libraries
    ```
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/your/cuda/lib64
    ```
    restart terminal or use `source ~/.bashrc` to use the new variables

## Install CuDNN
1. Download CuDnn from [Nvidia's page](https://developer.nvidia.com/rdp/cudnn-download) <br/>
   decompress the files
    ```
    tar -xvf cudnn-8.0-linux-x64-v5.0-ga.tgz
    ```
     
2. Copy the files into the system directory. 
    ```
    cd cudnn-8.0-linux-x64-v5.0-ga
    sudo cp -P include/cudnn.h /usr/include
    sudo cp -P lib64/libcudnn* /usr/lib/x86_64-linux-gnu/
    sudo chmod a+r /usr/lib/x86_64-linux-gnu/libcudnn*
    ```






